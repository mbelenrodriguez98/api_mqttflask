from flask import Flask, request, render_template, jsonify
from flask_mqtt import Mqtt
import json

app = Flask(__name__)
app.config['MQTT_BROKER_URL'] = 'localhost'  # use the free broker from HIVEMQ
app.config['MQTT_BROKER_PORT'] = 1883  # default port for non-tls connection
app.config['MQTT_USERNAME'] = ''  # set the username here if you need authentication for the broker
app.config['MQTT_PASSWORD'] = ''  # set the password here if the broker demands authentication
app.config['MQTT_KEEPALIVE'] = 5  # set the time interval for sending a ping to the broker to 5 seconds
app.config['MQTT_TLS_ENABLED'] = False  # set TLS to disabled for testing purposes

mqtt = Mqtt(app)

state_rele=0

@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    mqtt.subscribe('sensores/temperatura1')
    print('Connected')

@mqtt.on_message()
def handle_message(client, userdata, message):
    global temp_value
    print('on_message client : {} userdata :{} message.topic :{} message.payload :{}'.format(client, userdata, message.topic, message.payload.decode()))
    datos_ardu_in = json.loads(message.payload.decode())
    print('------Te llego algo-------')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/rele',methods=['POST','GET'])
def rele():
    global state_rele
    state_rele1 = request.args.get('rele1')
    state_rele2 = request.args.get('rele2')
    date = request.args.get('date')
    id = request.args.get('id')

#a partir de los datos creo un JSON

    data_dic = {
        "Reles" : ["Rele1":state_rele1,"Rele2":state_rele2],
        "Date" : date,
        "ID" : id
    }
    data_json = json.dumps(data_dic)

    mqtt.publish('activadores/rele1',data_json)

    return '''<h1>Se escribió en rele: {}</h1>'''.format(state_rele1)

@app.route('/temp')
def temp():
#    data = {
#        "id": 'ESP232',
#        "temp": temp_value,
#        "bat": 1,
#        "date": 20200604123030,
#        "loc": 1,
#        "state_r1": state_rele,
#        "state_r2": 'off',
#    }

#    data_json = json.dumps(data)

    return jsonify(datos_ardu_in)
    print ('Cayenne solicitó datos')
#    return render_template('index_2.html',data=data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
